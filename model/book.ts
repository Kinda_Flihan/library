export type Book = {
    id: number;

    title: string;

    description: string;

    price: number;

}

