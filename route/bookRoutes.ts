import express from 'express';
const router = express.Router();
const bookController = require("../Controller/bookController");

router.post('/',bookController.create);
router.get('/',bookController.getAll);
router.get('/:id', bookController.getById);
router.put('/:id', bookController.update);
router.delete('/:id', bookController.delete);
router.post('/CreateMany',bookController.createMany);

module.exports = router;