import express from 'express';
const router = express.Router();

const bookRoutes = require('./bookRoutes');

router.use('/book',bookRoutes);

module.exports=router;