const dbConnection = require('../configuration/sqlConfig');

let createBook = `create table if not exists books(
                          id int primary key auto_increment,
                          title varchar(50)not null,
                          description varchar(255) null,
                          price int not null
                      )`;

dbConnection.query(createBook, function(err, results, fields) {
    if (err) {
        console.log(err.message);
    }
});