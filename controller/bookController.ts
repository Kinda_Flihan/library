const bookRepository = require("../repository/bookRepository");

//create new book
exports.create = function (req , res) {
    //handles null error
    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        res.status(400).send({error: true, message: 'Please provide all required field'});
    } else {
        bookRepository.create(req.body, function (err, book) {
            if (err)
                res.send(err);
            res.json({message: "Added successfully!", data: book});
        });
    }
};

//Get book by id
exports.getById = function (req, res) {
    bookRepository.getById(req.params.id, function (err, book) {
        if (err)
            res.send(err);
        res.send(book);
    });
};

//update specific book
exports.update = function (req , res) {
    //handles null error
    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        res.status(400).send({error: true, message: 'Please provide all required field'});
    } else {
        bookRepository.update(req.params.id ,req.body, function (err, book) {
            if (err)
                res.send(err);
            res.json({message: "Updated successfully!", data: book});
        });
    }
};

//delete specific book by id
exports.delete = function (req, res) {
    bookRepository.delete(req.params.id, function (err, book) {
        if (err)
            res.send(err);
        res.send(book);
    });
};

//Get all books
exports.getAll = function (req, res) {
    bookRepository.getAll(function (err, books) {
        if (err)
            res.send(err);
        res.send(books);
    });
};

//create many books
exports.createMany = function (req , res) {
    //handles null error
    if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        res.status(400).send({error: true, message: 'Please provide all required field'});
    } else {
        bookRepository.createMany(req.body, function (err, book) {
            if (err)
                res.send(err);
            res.json({message: "Added successfully!", data: book});
        });
    }
};