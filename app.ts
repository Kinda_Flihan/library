import express, { Request, Response, NextFunction } from 'express';
import path from 'path';
import bodyParser from 'body-parser';


const app = express();
const port = 3000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));


//Create table if not exist
const sqlCreateTables = require('./mySql/SqlTables');


//prefix api for all routes
const router = require('./route/index');
app.use('/api', router);


app.listen(port, () => {
    console.log(`Server is running on port ${port}.`);
});