const dbConfig = require('../configuration/sqlConfig');


//create book
exports.create = async function (newBook, result) {
    dbConfig.query("INSERT INTO books set ?", newBook, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    })
}

exports.getById = async function (id, result){
    dbConfig.query("SELECT * from books WHERE id=? ", id, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    })
}

exports.update = async function (id, newBook, result){

    let data = [newBook, id];
    dbConfig.query("UPDATE books set ? WHERE id=?", data, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    })
}

exports.delete = async function (id, result){
    dbConfig.query("DELETE FROM books WHERE id=?", id, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    })
}

exports.getAll = async function (result){
    dbConfig.query("SELECT * from books ",function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    })
}

//create many books
exports.createMany = async function (listOfBooks, result) {

    // insert statement
    let stmt = `INSERT INTO books(title,description,price)  VALUES ?  `;
    let books = [];
    for (var i =0 ; i< listOfBooks.books.length ; i++){
        books.push([listOfBooks.books[i].title,listOfBooks.books[i].description, listOfBooks.books[i].price]);
    }

    dbConfig.query(stmt, [books], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    })
}
